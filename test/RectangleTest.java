import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RectangleTest {
    @Test
    void shouldReturnAreaAsEightWhenLengthIsTwoAndBreadthIsFour() {
        Rectangle rectangle=new Rectangle(2,4);

        int area=rectangle.area();

        assertEquals(8,area);
    }

    @Test
    void shouldReturnAreaAsTenWhenLengthIsFiveAndBreadthIsTwo() {
        Rectangle rectangle=new Rectangle(5,2);

        int area=rectangle.area();

        assertEquals(10,area);
    }
}